/**
 * Restricts a number between two bounds.
 *
 * @param n - The number to restrict.
 * @param min - The minimum bound.
 * @param max - The maximum bound.
 * @returns The number restricted between the specified bounds.
 */
export function clamp(n: number, min: number, max: number) {
  return Math.min(max, Math.max(min, n))
}
