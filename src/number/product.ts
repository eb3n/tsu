/**
 * Returns the product of a number array.
 *
 * @param ns - The number array.
 * @returns The product.
 */
export function product(ns: readonly number[]): number {
  if (ns.length < 1) return 0

  let total = 1

  for (let i = 0; i < ns.length; i++) {
    total *= ns[i]
  }

  return total
}
