/**
 * Returns the maximum number of provided numbers or array of numbers.
 *
 * @param ns - The numbers or the array of numbers.
 * @returns The maximum number.
 */
export function max(...ns: number[] | [number[]]): number {
  let numbers: number[]

  if (ns.length === 1 && Array.isArray(ns[0])) {
    numbers = ns[0]
  } else {
    numbers = ns as number[]
  }

  return Math.max(...numbers)
}
