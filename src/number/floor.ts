/**
 * Rounds a number down to the nearest multiple of the specified factor.
 *
 * @param n - The number to round down.
 * @param factor - The factor used for rounding.
 * @returns The rounded number.
 */
export function floor(n: number, factor = 1) {
  const quotient = n / factor

  return Math.floor(quotient) * factor
}
