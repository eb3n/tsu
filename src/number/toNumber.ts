/**
 * Converts a string which contains a number into the number itself.
 *
 * @param str - The string.
 * @param extensions - Additional symbols to consider in the conversion.
 * @returns The number.
 */
export function toNumber(str: string, extensions: string[] = []): number {
  if (!str.length) {
    return NaN
  }

  const symbols = ['#', '£', '€', '$', '%', ',', ...extensions]
  const re = new RegExp(`[${symbols.join('')}]`, 'g')

  return Number(str.replace(re, ''))
}
