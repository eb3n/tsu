import { randomNumber } from './randomNumber'

/**
 * Rolls an n-sided die.
 *
 * @param n - The number of sides on the die.
 * @returns If the die roll was 0.
 */
export function randomChance(n: number): boolean {
  return randomNumber(n) === 0
}
