/**
 * Rounds a number to the nearest multiple of the specified factor.
 *
 * @param n - The number to round.
 * @param factor - The factor used for rounding.
 * @returns The rounded number.
 */
export function round(n: number, factor = 1) {
  const quotient = n / factor

  return Math.round(quotient) * factor
}
