/**
 * Generates a random integer between bounds.
 *
 * @param min - The inclusive minimum bound.
 * @param max - The exclusive maximum bound.
 * @returns The random integer.
 */
export function randomNumber(max: number): number
export function randomNumber(min: number, max: number): number
export function randomNumber(...args: number[]): number {
  let min: number
  let max: number

  if (args.length === 1) {
    min = 0
    max = args[0]
  } else {
    min = args[0]
    max = args[1]
  }

  return Math.floor(Math.random() * (max - min)) + min
}
