import { Fn } from '~/types'

/**
 * Prevents function execution for a specified time period after it was last called.
 *
 * @param fn - The function.
 * @param limit - The time period.
 * @returns The throttled function.
 */
export function throttle(fn: Fn<void>, limit: number): Fn<void> {
  if (limit < 0) {
    console.warn('[tsu] warning: throttle limits less than 0 will be set to 0.')
  }

  let isWaiting = false

  return function (...args) {
    if (!isWaiting) {
      fn(...args)
      isWaiting = true

      setTimeout(() => {
        isWaiting = false
      }, limit)
    }
  }
}
