import { isArray, isNull, isObjectLike } from '../is'

/**
 * Deep clones an array or object.
 *
 * @param input - The array or object to clone.
 * @returns A deep clone of the input array or object.
 */
export function clone<T>(input: T): T {
  // arrays
  if (isArray(input)) {
    const clonedArray: any[] = []

    for (let i = 0; i < input.length; i++) {
      if (isObjectLike(input[i]) && !isNull(input[i])) {
        clonedArray[i] = clone(input[i])
      } else {
        clonedArray[i] = input[i]
      }
    }

    return clonedArray as any as T
  }

  // objects
  if (isObjectLike(input) && !isNull(input)) {
    const clonedObject: Record<string, any> = {}

    for (const key in input) {
      if (isObjectLike(input[key]) && !isNull(input[key])) {
        clonedObject[key] = clone(input[key])
      } else {
        clonedObject[key] = input[key]
      }
    }

    return clonedObject as T
  }

  return input
}
