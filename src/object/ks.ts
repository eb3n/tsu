/**
 * Strict-typed, shorthand `Object.keys`
 *
 * @param obj - The object to extract the keys from.
 * @returns The keys.
 */
export function ks<T extends object>(
  obj: T
): Array<`${keyof T & (string | number | boolean | null | undefined)}`> {
  return Object.keys(obj) as Array<`${keyof T & (string | number | boolean | null | undefined)}`>
}
