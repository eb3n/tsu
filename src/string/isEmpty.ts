/**
 * Identifies empty strings.
 *
 * @param str - The string.
 * @returns If the string is empty.
 */
export function isEmpty(str: string): boolean {
  return str.length === 0
}
