/**
 * Joins an array of characters into a string.
 *
 * @param chars - The characters.
 * @returns The string.
 */
export function unchars(chars: string[]): string {
  return chars.join('')
}
