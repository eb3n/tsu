/**
 * Adds the respective ordinal suffix (-st, -nd, -rd or -th) to a number.
 *
 * @param n - The number.
 * @returns The number with the ordinal suffix appended.
 */
export function toOrdinal(n: number): string {
  const suffixes = ['st', 'nd', 'rd']
  const suffix = suffixes[((Math.abs(n) / 10) % 10 ^ 1 && Math.abs(n) % 10) - 1] || 'th'

  return `${n}${suffix}`
}
