/**
 * Splits a string into two at a specified index.
 *
 * @param str - The string.
 * @param i - The position to split at.
 * @returns The tuple of strings before and after the split.
 */
export function splitString(str: string, i: number): [string, string] {
  if (i <= 0) {
    return ['', str]
  }

  if (i > str.length) {
    return [str, '']
  }

  return [str.slice(0, i), str.slice(i)]
}
