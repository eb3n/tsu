/**
 * Identifies if a value is a string.
 *
 * @param val - The value.
 * @returns If the value is a string.
 */
export function isString(val: unknown): val is string {
  return typeof val === 'string'
}
