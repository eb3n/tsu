import { Obj } from '~/types'

/**
 * Identifies if a value is an object.
 *
 * @param val - The value.
 * @returns If the value is an object.
 */
export function isObject(val: unknown): val is Obj {
  return Object.prototype.toString.call(val) === '[object Object]'
}
