/**
 * Identifies if a value is defined.
 *
 * @param val - The value.
 * @returns If the value is defined.
 */
export function isDefined<T>(val: T): val is Exclude<T, null | undefined> {
  return typeof val !== 'undefined'
}
