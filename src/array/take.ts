/**
 * Returns the first `n` items of an array.
 *
 * @param array - The array.
 * @param n - The number of items to return.
 * @returns The first `n` items.
 */
export function take<T>(array: readonly T[], n: number): T[] {
  if (n <= 0) {
    return []
  }

  return array.slice(0, n)
}
