/**
 * Splits an array into groups of equal size.
 *
 * @param array - The array.
 * @param n - The size of each group.
 * @returns The array split into groups.
 */
export function groups<T>(array: readonly T[], n: number): T[][] {
  if (n <= 0) {
    throw new Error('[tsu] Invalid group size. Is it greater than 0?')
  }

  if (n >= array.length) {
    return [[...array]]
  }

  const grouped = []

  for (let i = 0; i < array.length; i += n) {
    grouped.push(array.slice(i, i + n))
  }

  return grouped
}
