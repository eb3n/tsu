/**
 * Returns the first item of an array.
 *
 * @param array - The array.
 * @returns The first item.
 */
export function head(array: readonly []): undefined
export function head<T>(array: readonly T[]): T
export function head<T>(array: readonly T[] | readonly []): T | undefined {
  return array[0]
}
