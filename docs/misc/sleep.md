# sleep

> Halts thread execution for a specified time period.

## Usage

```ts
import { sleep } from 'tsu'

sleep(1000).then(() => {
  // ...
})
// pauses execution for 1000ms

await sleep(1000)
// pauses execution for 1000ms
```

## Type Definitions

```ts
/**
 * @param duration - The time period (in ms).
 * @returns The halting promise.
 */
export declare async function sleep(duration: number): Promise<void>
```
