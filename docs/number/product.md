# product

> Returns the product of a number array.

## Usage

```ts
import { product } from 'tsu'

product([1, 2, 3, 4, 5])
// 120

product([])
// 0
```

## Type Definitions

```ts
/**
 * @param ns - The number array.
 * @returns The product.
 */
function product(ns: readonly number[]): number
```
