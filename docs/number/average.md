# average

> Returns the average of a number array.

## Usage

```ts
import { average } from 'tsu'

average([1, 2, 3, 4, 5])
// 3

average([])
// 0
```

## Type Definitions

```ts
/**
 * @param ns - The number array.
 * @returns The average.
 */
function average(ns: readonly number[]): number
```
