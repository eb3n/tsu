# toNumber

> Converts a string which contains a number into the number itself.

## Usage

:::warning
`toNumber` removes the following symbols by default: **, # £ € $ %**

Any symbols provided to the `extensions` parameter will be removed **in addition** to these default symbols.
:::

```ts
import { toNumber } from 'tsu'

toNumber('1234')
// 1234

toNumber('$1,234')
// 1234

toNumber('@12^34§', ['@', '^', '§'])
// 1234

toNumber('ribbit')
// NaN
```

## Type Definitions

```ts
/**
 * @param str - The string.
 * @param extensions - Additional symbols to consider in the conversion.
 * @returns The number.
 */
function toNumber(str: string, extensions: string[] = []): number
```
