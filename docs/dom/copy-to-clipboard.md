# copyToClipboard

> Copies the provided string to the user's clipboard.

## Usage

```ts
import { copyToClipboard } from 'tsu'

const text = 'ribbit'
copyToClipboard(text)
// 'ribbit' is now on the user's clipboard
```

## Type Definitions

```ts
/**
 * @param str - The string to be copied to the clipboard.
 */
function copyToClipboard(str: string): void
```
