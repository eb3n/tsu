# tail

> Returns all but the first item of an array.

## Usage

```ts
import { tail } from 'tsu'

tail(['a', 'b', 'c', 'd', 'e'])
// ['b', 'c', 'd', 'e']
```

## Type Definitions

```ts
/**
 * @param array - The array.
 * @returns The array without the first item.
 */
function tail<T>(array: readonly T[]): T[]
```
