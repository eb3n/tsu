# groups

> Splits an array into groups of equal size.

## Usage

```ts
import { groups } from 'tsu'

groups(['a', 'b', 'c', 'd', 'e'], 2)
// [['a', 'b'], ['c', 'd'], ['e']]
```

## Type Definitions

```ts
/**
 * @param array - The array.
 * @param n - The size of each group.
 * @returns The array split into groups.
 */
function groups<T>(array: readonly T[], n: number): T[][]
```
