# vs

> Shorthand `Object.values`

## Usage

```ts
import { vs } from 'tsu'

const obj = { foo: 'bar', baz: 42 }

vs(obj)
// ['bar', 42]
```

## Type Definitions

```ts
/**
 * @param obj - The object to extract the values from.
 * @returns The values.
 */
function vs<T>(obj: Obj<T>): T[]
```
