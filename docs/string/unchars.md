# unchars

> Joins an array of characters into a string.

## Usage

```ts
import { unchars } from 'tsu'

unchars(['r', 'i', 'b', 'b', 'i', 't'])
// 'ribbit'
```

## Type Definitions

```ts
/**
 * @param chars - The characters.
 * @returns The string.
 */
function unchars(chars: string[]): string
```
