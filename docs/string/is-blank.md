# isBlank

> Identifies strings which only contain whitespace.

## Usage

```ts
import { isBlank } from 'tsu'

isBlank('ribbit')
// false

isBlank('   ')
// true

isBlank('')
// true
```

## Type Definitions

```ts
/**
 * @param str - The string.
 * @returns If the string is blank.
 */
function isBlank(str: string): boolean
```
