# isObjectLike

> Identifies if a value is an array, an object, a date, or null.

## Type Definitions

```ts
/**
 * @param val - The value.
 * @returns If the value is an array, an object, a date, or null.
 */
function isObjectLike(val: unknown): val is object
```
