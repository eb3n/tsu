# isEmptyArray

> Identifies if a value is an empty array.

## Type Definitions

```ts
/**
 * @param val - The value.
 * @returns If the value is an empty array.
 */
function isEmptyArray(val: unknown): val is []
```
