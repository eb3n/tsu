# isArray

> Identifies if a value is an array.

## Type Definitions

```ts
/**
 * @param val - The value.
 * @returns If the value is an array.
 */
function isArray(val: unknown): val is any[]
```
