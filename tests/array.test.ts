import { describe, expect, test } from 'vitest'
import * as A from '../src/array'

describe('array: drop', () => {
  test('should return an empty array if provided an empty array', () => {
    const array: [] = []
    const items = A.drop(array, 3)

    expect(items).toEqual(array)
  })

  test('should return the array, without the first n items', () => {
    const array = [1, 2, 3, 4, 5]
    const items = A.drop(array, 3)

    expect(items).toEqual([4, 5])
  })

  test('should return the whole array if (n < 0)', () => {
    const array = [1, 2, 3, 4, 5]
    const items = A.drop(array, -1)

    expect(items).toEqual([1, 2, 3, 4, 5])
  })

  test('should return the whole array if (n = 0)', () => {
    const array = [1, 2, 3, 4, 5]
    const items = A.drop(array, 0)

    expect(items).toEqual([1, 2, 3, 4, 5])
  })

  test('should return an empty array if (n > array.length)', () => {
    const array = [1, 2, 3, 4, 5]
    const items = A.drop(array, 100)

    expect(items).toEqual([])
  })
})

describe('array: groups', () => {
  test('should throw an error if given an invalid group size', () => {
    const array: [] = []

    expect(() => {
      A.groups(array, 0)
    }).toThrowError('[tsu] Invalid group size. Is it greater than 0?')
  })

  test('should return the original array, wrapped in an array, if group size is the same size as the original array', () => {
    const array = [1, 2, 3, 4, 5]
    const items = A.groups(array, 5)

    expect(items).toEqual([[1, 2, 3, 4, 5]])
  })

  test('should return the array, grouped into groups of n', () => {
    const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    const items = A.groups(array, 3)

    expect(items).toEqual([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10]])
  })
})

describe('array: head', () => {
  test('should return undefined if provided an empty array', () => {
    const array: [] = []
    const head = A.head(array)

    expect(head).toBe(undefined)
  })

  test('should return the first item of an array', () => {
    const array = [1, 2, 3]
    const head = A.head(array)

    expect(head).toBe(1)
  })
})

describe('array: init', () => {
  test('should return an empty array if provided an empty array', () => {
    const array: [] = []
    const init = A.init(array)

    expect(init).toEqual([])
  })

  test('should return an empty array if provided a singleton array', () => {
    const array = [1]
    const init = A.init(array)

    expect(init).toEqual([])
  })

  test('should return all elements of an array, except the last', () => {
    const array = [1, 2, 3]
    const init = A.init(array)

    expect(init).toEqual([1, 2])
  })
})

describe('array: last', () => {
  test('should return undefined if provided an empty array', () => {
    const array: [] = []
    const last = A.last(array)

    expect(last).toBe(undefined)
  })

  test('should return the last item of an array', () => {
    const array = [1, 2, 3]
    const last = A.last(array)

    expect(last).toBe(3)
  })
})

describe('array: partition', () => {
  test('should return two empty arrays if provided an empty array', () => {
    const array: [] = []
    const result = A.partition(array, (x) => Boolean(x % 2))

    expect(result).toEqual([[], []])
  })

  test("should return two arrays: the first containing values which pass the filter, the second containing values that don't", () => {
    const array = [1, 2, 3, 4, 5]
    const result = A.partition(array, (x) => !(x % 2))

    expect(result).toEqual([
      [2, 4],
      [1, 3, 5]
    ])
  })
})

describe('array: range', () => {
  test('should return a range of numbers from 0 to the specified, exlusive number', () => {
    const array = A.range(5)

    expect(array).toEqual([0, 1, 2, 3, 4])
  })

  test('should return an empty array if stop value is 0', () => {
    const array = A.range(0)

    expect(array).toEqual([])
  })

  test('should return an empty array if stop value is less than 0', () => {
    const array = A.range(-1)

    expect(array).toEqual([])
  })

  test('should return a range of numbers between the specified bounds', () => {
    const array = A.range(1, 10)

    expect(array).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9])
  })

  test('should return a range of numbers between the specified bounds, each the specified step size apart', () => {
    const array = A.range(1, 10, 2)

    expect(array).toEqual([1, 3, 5, 7, 9])
  })

  test('should return an empty array if the start value equals the stop value', () => {
    const array = A.range(10, 10)

    expect(array).toEqual([])
  })

  test('should return an empty array if the start value exceeds the stop value', () => {
    const array = A.range(15, 10)

    expect(array).toEqual([])
  })

  test('should return a range of numbers between the specified bounds, each 1 apart, if the step value equals 0', () => {
    const array = A.range(1, 10, 0)

    expect(array).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9])
  })

  test('should return a range of numbers between the specified bounds, each 1 apart, if the step value is less than 0', () => {
    const array = A.range(1, 10, -1)

    expect(array).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9])
  })

  test('should return an array containing the start value if the step exceeds the difference between the start and stop values', () => {
    const array = A.range(1, 10, 99)

    expect(array).toEqual([1])
  })
})

describe('array: splitArray', () => {
  test('should return the two parts of the array, either side of a specified position', () => {
    const array = [1, 2, 3, 4, 5]
    const result = A.splitArray(array, 2)

    expect(result).toEqual([
      [1, 2],
      [3, 4, 5]
    ])
  })

  test('should return two empty arrays if provided an empty array', () => {
    const array: [] = []
    const result = A.splitArray(array, 1)

    expect(result).toEqual([[], []])
  })

  test('should return an empty array and the whole array if the split index is less than 0', () => {
    const array = [1, 2, 3, 4, 5]
    const result = A.splitArray(array, -1)

    expect(result).toEqual([[], [1, 2, 3, 4, 5]])
  })

  test('should return an empty array and the whole array if the split index equals 0', () => {
    const array = [1, 2, 3, 4, 5]
    const result = A.splitArray(array, 0)

    expect(result).toEqual([[], [1, 2, 3, 4, 5]])
  })

  test('should return the whole array and an empty array if the split index exceeds the length of the array', () => {
    const array = [1, 2, 3, 4, 5]
    const result = A.splitArray(array, 100)

    expect(result).toEqual([[1, 2, 3, 4, 5], []])
  })
})

describe('array: tail', () => {
  test('should return an empty array if provided an empty array', () => {
    const array: [] = []
    const tail = A.tail(array)

    expect(tail).toEqual([])
  })

  test('should return an empty array if provided a singleton array', () => {
    const array = [1]
    const tail = A.tail(array)

    expect(tail).toEqual([])
  })

  test('should return all elements of an array, except the first', () => {
    const array = [1, 2, 3]
    const tail = A.tail(array)

    expect(tail).toEqual([2, 3])
  })
})

describe('array: take', () => {
  test('should return an empty array if provided an empty array', () => {
    const array: [] = []
    const items = A.take(array, 3)

    expect(items).toEqual([])
  })

  test('should return the first n items from an array', () => {
    const array = [1, 2, 3, 4, 5]
    const items = A.take(array, 3)

    expect(items).toEqual([1, 2, 3])
  })

  test('should return an empty array if (n < 0)', () => {
    const array = [1, 2, 3, 4, 5]
    const items = A.take(array, -1)

    expect(items).toEqual([])
  })

  test('should return an empty array if (n = 0)', () => {
    const array = [1, 2, 3, 4, 5]
    const items = A.take(array, 0)

    expect(items).toEqual([])
  })

  test('should return the whole array if (n > array.length)', () => {
    const array = [1, 2, 3, 4, 5]
    const items = A.take(array, 100)

    expect(items).toEqual([1, 2, 3, 4, 5])
  })
})

describe('array: toArray', () => {
  test('should wrap a single value in an array', () => {
    const value = 1
    const array = A.toArray(value)

    expect(array).toEqual([1])
  })

  test('should return the same value if it is already an array', () => {
    const value = [1, 2, 3]
    const array = A.toArray(value)

    expect(array).toEqual([1, 2, 3])
  })

  test('should return an empty array if the provided value is nullable', () => {
    const value = null
    const array = A.toArray(value)

    expect(array).toEqual([])
  })
})

describe('array: uniq', () => {
  test('should return an empty array if provided an empty array', () => {
    const original: [] = []
    const uniques = A.uniq(original)

    expect(uniques).toEqual([])
  })

  test('should remove duplicate values from an array', () => {
    const original = [1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5]
    const uniques = A.uniq(original)

    expect(uniques).toEqual([1, 2, 3, 4, 5])
  })

  test("shouldn't remove items from an already unique array", () => {
    const original = [1, 2, 3, 4, 5]
    const uniques = A.uniq(original)

    expect(uniques).toEqual([1, 2, 3, 4, 5])
  })
})
